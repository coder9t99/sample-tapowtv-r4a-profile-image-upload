//
//  ViewController.m
//  fileupload
//
//  Created by coder on 13/12/17.
//  Copyright © 2017 noone. All rights reserved.
//

#import "ViewController.h"
#import "AFNetworking.h"

@interface ViewController ()
@property (strong) AFURLSessionManager * manager;
@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    self.manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    [self deviceAuth];
}

- (void)deviceAuth {
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:configuration];

    NSURL *URL = [NSURL URLWithString:@"http://pp-dev.r4a.tapow.tv/api/devices?apiKey=d414ea0a5d5a4f89a747d6cfcd9a098d&manufacturer=hp&model=wk&udid=1234567"];
    NSURLRequest *request = [NSURLRequest requestWithURL:URL];

    NSURLSessionDataTask *dataTask = [manager dataTaskWithRequest:request completionHandler:^(NSURLResponse *response, id responseObject, NSError *error) {
        if (error) {
            NSLog(@"Error: %@", error);
        } else {
            NSLog(@"%@ %@", response, responseObject);
            [self login];
        }
    }];
    [dataTask resume];
}

- (void)login {
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:configuration];

    NSURL *URL = [NSURL URLWithString:@"http://pp-dev.r4a.tapow.tv/api/sg/16/users?email=coder9t99@tapow.com&password=N%2FWqFuztgw0%2BSnHZ&GCMID=xx&registration_id=someRegId&device_name=someDevice&device_os=someOs&ostype=someOsType&imei=someImei&version=someVersion&device_finger_print=someFingerPrint"];
    NSURLRequest *request = [NSURLRequest requestWithURL:URL];

    NSURLSessionDataTask *dataTask = [manager dataTaskWithRequest:request completionHandler:^(NSURLResponse *response, id responseObject, NSError *error) {
        if (error) {
            NSLog(@"Error: %@", error);
        } else {
            NSLog(@"%@ %@", response, responseObject);
            [self upload];
        }
    }];
    [dataTask resume];
}

- (void)upload {
    UIImage *image = [UIImage imageNamed:@"pirate-cat.jpg"];
    NSData *imageData = UIImageJPEGRepresentation(image, 0.5);

    NSMutableURLRequest *request= [[NSMutableURLRequest alloc] init];
    [request setURL:[NSURL URLWithString:@"http://pp-dev.r4a.tapow.tv/api/sg/16/Users"]];
    [request setHTTPMethod:@"POST"];
    [request addValue:@"multipart/form-data" forHTTPHeaderField: @"Content-Type"];
    [request setHTTPBody:imageData];

    NSURLSessionUploadTask *uploadTask;
    uploadTask = [self.manager
            uploadTaskWithStreamedRequest:request
                                 progress:^(NSProgress * _Nonnull uploadProgress) {
                                     dispatch_async(dispatch_get_main_queue(), ^{
                                         NSLog(@"progress: %@", uploadProgress);
                                     });
                                 }
                        completionHandler:^(NSURLResponse * _Nonnull response, id  _Nullable responseObject, NSError * _Nullable error) {
                            if (error) {
                                NSLog(@"Error: %@", error);
                            } else {
                                NSLog(@"::Image Uploaded::\n%@ %@", response, responseObject);
                                // check this URL for uploaded file.
                                // https://s3-ap-southeast-1.amazonaws.com/r4a.tapowtv.user-profile-image/UserProfileImages/981.jpg
                            }
                        }];

    [uploadTask resume];
}

@end
