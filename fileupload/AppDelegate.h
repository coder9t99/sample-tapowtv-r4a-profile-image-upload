//
//  AppDelegate.h
//  fileupload
//
//  Created by coder on 13/12/17.
//  Copyright © 2017 noone. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end
